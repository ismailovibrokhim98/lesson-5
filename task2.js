
    //Fetch and then
    const promise = fetch('https://jsonplaceholder.typicode.com/users')
    const response = promise.then(response => response.json())
    const data = response.then((data) => console.log('data', data))
    
    
    // async await
    
    const getData = async function() {
      const promise = await fetch('https://jsonplaceholder.typicode.com/posts')
      const response = await promise.json()
      console.log(response)
    }
    
    
    console.log(getData())
    